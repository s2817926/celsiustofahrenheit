package nl.utwente.di.temp;


public class Convertor {

    public Convertor() { }

    public double toFahrenheit(double celsius) {
        return (celsius * 9) / 5 + 32;
    }

}
