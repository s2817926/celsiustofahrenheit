package nl.utwente.di.temp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemp {

    @Test
    public void testConvert() {
        Convertor convertor = new Convertor();
        double temp = convertor.toFahrenheit(30.0);
        Assertions.assertEquals(86.0, temp, 0.0, "First attempt");
    }

}